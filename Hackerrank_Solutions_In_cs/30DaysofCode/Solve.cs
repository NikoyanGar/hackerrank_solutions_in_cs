﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hackerrank_Solutions_In_cs._30DaysofCode
{
    class Solve
    {
        static void Day8DictionariesandMaps()
        {
            int n = int.Parse(Console.ReadLine());
            Dictionary<string, string> PhoneBook = new Dictionary<string, string>(n);
            for (int i = 0; i < n; i++)
            {
                string str = Console.ReadLine().Replace(" ", "");
                string name = new string(str.TakeWhile(ch => Char.IsLetter(ch)).ToArray());
                string phoneNumber = new string(str.SkipWhile(ch => Char.IsLetter(ch)).ToArray());
                PhoneBook.Add(name, phoneNumber);
            }
            while (true)
            {
                string name = Console.ReadLine();
                if (name == null)
                {
                    break;
                }

                if (PhoneBook.ContainsKey(name))
                {
                    Console.WriteLine($"{name}={PhoneBook[name]}");
                }
                else
                {
                    Console.WriteLine("Not found");
                }
            }
        }
        static int factorial(int n)
        {
            if (n == 1)
            {
                return 1;
            }
            else
            {
                return n * factorial(n - 1);
            }
        }
        static void Day10BinaryNumbers()
        {
            //Алгоритм перевода чисел из одной системы счисления в другую
            //https://math.semestr.ru/inf/index.php
            var n = int.Parse(Console.ReadLine());

            var count = 0;
            var max = 0;

            while (n > 0)
            {
                if (n % 2 == 1)
                {
                    count++;

                    if (count > max)
                        max = count;
                }
                else count = 0;

                n = n / 2;
            }

            Console.WriteLine(max);
        }
        static void Day11Arrays(string[] args)
        {
            int[][] arr = new int[6][];

            for (int i = 0; i < 6; i++)
            {
                arr[i] = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp));
            }
            int max = int.MinValue;

            for (int i = 1; i < 5; i++)
            {
                for (int j = 1; j < 5; j++)
                {
                    int temp = arr[i][j] + arr[i - 1][j] + arr[i - 1][j + 1] + arr[i - 1][j - 1]
                        + arr[i + 1][j] + arr[i + 1][j + 1] + arr[i + 1][j - 1];
                    if (temp > max)
                    {
                        max = temp;
                    }
                }
            }
            Console.WriteLine(max);
        }
        static void Day16(String[] args)
        {
            string S = Console.ReadLine();
            int x;
            try
            {
                x = int.Parse(S);
                Console.WriteLine(x);
            }
            catch
            {
                Console.WriteLine("Bad String");
            }
        }
        class Calculator
        {


            internal int power(int n, int p)
            {
                if (n < 0 || p < 0)
                {
                    throw new Exception("n and p should be non-negative");
                }
                else
                {
                    return (int)Math.Pow(n, p);
                }
            }
        }//day 17
        #region Day18
        Stack<char> stChars = new Stack<char>();
        Queue<char> queChars = new Queue<char>();
        private char dequeueCharacter()
        {
            return queChars.Dequeue();
        }

        private char popCharacter()
        {
            return stChars.Pop();
        }

        private void enqueueCharacter(char c)
        {
            queChars.Enqueue(c);
        }

        private void pushCharacter(char c)
        {
            stChars.Push(c);
        }

        #endregion
        #region Day19
        public interface AdvancedArithmetic
        {
            int divisorSum(int n);
        }

        public class Calculator1 : AdvancedArithmetic//rename Calculator1=>Calculator
        {
            public int divisorSum(int n)
            {
                int sum = 0;
                for (int i = 1; i <= n / 2; i++)
                {
                    if (n % i == 0)
                    {
                        sum += i;
                    }
                }
                return sum += n;
            }
        }
        #endregion
        #region Day 20
        class Solution
        {

            static void Main1(String[] args)//rename Main1=>Main
            {
                int n = Convert.ToInt32(Console.ReadLine());
                string[] a_temp = Console.ReadLine().Split(' ');
                int[] a = Array.ConvertAll(a_temp, Int32.Parse);
                int count = Sort(a);
                Console.WriteLine($"Array is sorted in {count} swaps.");
                Console.WriteLine($"First Element: {a[0]}");
                Console.WriteLine($"Last Element: {a[a.Length - 1]}");
            }
            static int Sort(int[] arr)
            {
                int numberOfSwaps = 0;

                for (int i = 0; i < arr.Length; i++)
                {
                    bool swaped = false;
                    for (int j = 0; j < arr.Length - 1 - i; j++)
                    {
                        if (arr[j] > arr[j + 1])
                        {
                            swap(arr, j, j + 1);
                            swaped = true;
                            numberOfSwaps++;
                        }
                    }

                    if (swaped == false)
                    {
                        break;
                    }
                }
                return numberOfSwaps;

            }

            private static void swap(int[] sourse, int p1, int p2)
            {
                int temp = sourse[p1];
                sourse[p1] = sourse[p2];
                sourse[p2] = temp;
            }
        }
        #endregion
        //Day 21
        static void PrintArray<T>(System.Collections.Generic.IEnumerable<T> source) //System.Collections.Generic.IEnumerable<T> source => T[] source
        {
            foreach (var item in source)
            {
                Console.WriteLine(item);
            }
        }
        //Day 22
        //public static int getHeight(Node root)
        //{
        //    int heightLeft = 0;
        //    int heightRight = 0;

        //    if (root.left != null)
        //    {
        //        heightLeft = getHeight(root.left) + 1;
        //    }
        //    if (root.right != null)
        //    {
        //        heightRight = getHeight(root.right) + 1;
        //    }

        //    return (heightLeft > heightRight ? heightLeft : heightRight);
        //}
        #region Day 23
        //static void levelOrder(Node root)
        //{
        //    if (root != null)
        //    {
        //        Queue<Node> queue = new Queue<Node>();
        //        queue.Enqueue(root);

        //        while (queue.Count > 0)
        //        {
        //            Node node = queue.Dequeue();

        //            Console.Write($"{node.data} ");

        //            if (node.left != null)
        //            {
        //                queue.Enqueue(node.left);
        //            }
        //            if (node.right != null)
        //            {
        //                queue.Enqueue(node.right);
        //            }
        //        }
        //    }
        //}
        #endregion 
        #region Day 24
        class Node
        {
            public int data;
            public Node next;
            public Node(int d)
            {
                data = d;
                next = null;
            }

        }
        class Day24
        {
            //runtime error(test case 1,4)
            public static Node removeDuplicates(Node head)
            {
                Node currentNode = head;
                while (currentNode.next != null)
                {
                    Node node = currentNode;
                    while (node.next != null)
                    {
                        if (node.next.data == currentNode.data)
                        {
                            node.next = node.next.next;
                        }
                        else
                        {
                            node = node.next;
                        }
                    }
                    currentNode = currentNode.next;
                }
                return head;
            }

            public static Node removeDuplicates1(Node head)
            {
                if (head == null || head.next == null)
                {
                    return head;
                }
                if (head.data == head.next.data)
                {
                    head.next = head.next.next;
                    removeDuplicates(head);
                }
                else
                {
                    removeDuplicates(head.next);
                }
                return head;
            }
        }
        #endregion
        #region Day 25
        static void Main1()
        {

            int n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                int current = int.Parse(Console.ReadLine());
                if (IsPrime(current))
                    Console.WriteLine("Prime");
                else
                    Console.WriteLine("Not prime");
            }

            Console.WriteLine();

        }

        static bool IsPrime(int source)
        {
            if (source == 1)
                return false;
            double sqrt = Math.Sqrt(source);
            for (int j = 2; j <= sqrt; j++)
            {
                if (source % j == 0)
                {
                    return false;
                }
            }
            return true;
        }
        #endregion
        #region Day 26
        public void Fine()
        {
            int fine = 0;
            int[] actual = Console.ReadLine().Split().Select(int.Parse).ToArray();
            int[] expected = Console.ReadLine().Split().Select(int.Parse).ToArray();


            if (actual[2] > expected[2])
                fine = 10000;

            else if (actual[1] > expected[1] && actual[2] == expected[2])
                fine = (actual[1] - expected[1]) * 500;

            else if (actual[0] > expected[0] && actual[2] == expected[2] && actual[1] == expected[1])
                fine = (actual[0] - expected[0]) * 15;


            Console.WriteLine(fine);
        }
        #endregion
    }

    #region Day 12
    class Person
    {
        protected string firstName;
        protected string lastName;
        protected int id;

        public Person() { }
        public Person(string firstName, string lastName, int identification)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.id = identification;
        }
        public void printPerson()
        {
            Console.WriteLine("Name: " + lastName + ", " + firstName + "\nID: " + id);
        }
    }
    class Student : Person
    {
        private int[] testScores;

        public Student(string FN, string LN, int I, int[] scores) : base(FN, LN, I)
        {
            this.testScores = scores;
        }

        public char Calculate()
        {
            int avg = 0;
            int sum = 0;

            for (int i = 0; i < testScores.Length; i++)
            {
                sum += testScores[i];
            }

            int n = testScores.Length;
            avg = sum / n;


            if (avg >= 90 && avg <= 100)
                return ('O');
            else if (avg >= 80 && avg <= 90)
                return ('E');
            else if (avg >= 70 && avg <= 80)
                return ('A');
            else if (avg >= 55 && avg <= 70)
                return ('P');
            else if (avg >= 40 && avg <= 55)
                return ('D');
            else if (avg < 40)
                return ('T');
            return ('X');
        }
    }
    #endregion
    #region Day 13
    abstract class Book
    {

        protected String title;
        protected String author;

        public Book(String t, String a)
        {
            title = t;
            author = a;
        }
        public abstract void display();


    }
    class MyBook : Book
    {
        public int Price { get; set; }
        public MyBook(string title, string author, int price) : base(title, author)

        {
            Price = price;
        }

        public override void display()
        {
            Console.WriteLine($"Title: {title}");
            Console.WriteLine($"Author: {author}");
            Console.WriteLine($"Price: {Price}");
        }
    }
    #endregion
    #region Day 14
    class Difference
    {
        private int[] elements;
        public int maximumDifference;
        public Difference(int[] arr)
        {
            elements = arr;
        }

        public void computeDifference()
        {
            Array.Sort(elements);
            maximumDifference = elements[elements.Length - 1] - elements[0];
        }
    }
    #endregion
    #region Day 15
    class Node
    {
        public int data;
        public Node next;
        public Node(int d)
        {
            data = d;
            next = null;
        }

    }
    class Solution
    {

        public static Node insert(Node head, int data)
        {
            if (head == null)
            {
                return new Node(data);
            }

            Node tmp = head;
            while (tmp.next != null)
            {
                tmp = tmp.next;
            }
            tmp.next = new Node(data);

            return head;
        }
    }
        #endregion
    }

