﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Hackerrank_Solutions_In_cs.InterviewPreparationKit
{
    class StringManipulation
    {
        static int makeAnagram(string a, string b)
        {

            int count = 0;
            var dicta = a.GroupBy(ch => ch).ToDictionary(ch => ch.Key, ch => ch.Count());
            var dictb = b.GroupBy(ch => ch).ToDictionary(ch => ch.Key, ch => ch.Count());
            var equalKeysDif = dicta.Join(dictb, da => da.Key, db => db.Key, (da, db) => new
            {
                dif = Math.Abs(da.Value - db.Value)
            });
            var difKeys = dicta.Keys.Except(dictb.Keys).Concat(dictb.Keys.Except(dicta.Keys));
            //  var dk = dicta.Keys.Concat(dictb.Keys).Distinct();
            var dictc = dicta.Concat(dictb);
            foreach (var key in difKeys)
            {
                foreach (var item in dictc)
                {
                    if (item.Key == key)
                    {
                        count += item.Value;
                    }
                }
            }
            foreach (var item in equalKeysDif)
            {
                count += item.dif;
            }

            return count;

        }

        #region Alternating Characters
        static int alternatingCharacters(string s)
        {
            int count = 0;
            for (int i = 0; i < s.Length - 1; i++)
            {
                if (s[i] == s[i + 1])
                {
                    count++;
                }
            }
            return count;

        }

    //    static void Main(string[] args)
    //    {
    //        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

    //        int q = Convert.ToInt32(Console.ReadLine());

    //        for (int qItr = 0; qItr < q; qItr++)
    //        {
    //            string s = Console.ReadLine();

    //            int result = alternatingCharacters(s);

    //            textWriter.WriteLine(result);
    //        }

    //        textWriter.Flush();
    //        textWriter.Close();
    //    }
     #endregion
    }
}
