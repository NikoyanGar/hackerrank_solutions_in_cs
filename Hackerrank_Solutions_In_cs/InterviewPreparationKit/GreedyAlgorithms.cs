﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hackerrank_Solutions_In_cs.InterviewPreparationKit
{
    class GreedyAlgorithms
    {
        #region
        static int luckBalance(int k, int[][] contests)
        {

            int lucky = 0;
            var x = contests.OrderByDescending(c => c[0]);
            foreach (var item in x)
            {
                if (item[1] == 0)
                {
                    lucky += item[0];
                }
                else
                {
                    if (k != 0)
                    {
                        if (item[1] == 1)
                        {
                            k--;
                        }
                        lucky += item[0];
                    }
                    else
                    {
                        lucky -= item[0];

                    }
                }

            }

            return lucky;
        }

        //code optimization,luckBalance1 =>luckBalance
        static int luckBalance1(int k, int[][] contests)
        {

            int lucky = contests.Where(c => c[1] == 0).Select(c => c[0]).Sum();
            var x = contests.Where(c => c[1] == 1).Select(c => c[0]).OrderByDescending(c => c);
            foreach (var item in x)
            {
                if (k != 0)
                {
                    k--;
                    lucky += item;
                }
                else
                {
                    lucky -= item;

                }
            }

            return lucky;
        }
        #endregion

        static int minimumAbsoluteDifference(int[] arr)
        {

            if (arr.Length != arr.Distinct().ToArray().Length)//for case 2(lenght=100000,dif=0)
            {
                return 0;
            }
            var arr1 = arr.OrderBy(a => a).ToArray();
            int min = Math.Abs(arr1[1] - arr1[0]);

            for (int i = 1; i < arr.Length - 1; i++)
            {
                int dif = Math.Abs(arr1[i + 1] - arr1[i]);
                if (dif < min)
                {
                    min = dif;
                }
            }

            return min;

        }
        static int maxMin(int k, int[] arr)
        {

            var x = arr.GroupBy(a => a).ToDictionary(a => a.Key, a => a.Count()).OrderByDescending(d => d.Value).FirstOrDefault();
            if (x.Value >= k)
            {
                return 0;
            }
            var arr1 = arr.OrderBy(a => a).ToArray();
            int min = Math.Abs(arr1[k - 1] - arr1[0]);

            for (int i = 1; i < arr.Length - k + 1; i++)
            {
                int dif = Math.Abs(arr1[i + k - 1] - arr1[i]);
                if (dif < min)
                {
                    min = dif;
                }
            }

            return min;

        }
    }
}
