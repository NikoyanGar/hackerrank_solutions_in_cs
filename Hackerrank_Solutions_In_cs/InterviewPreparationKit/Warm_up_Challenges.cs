﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hackerrank_Solutions_In_cs.InterviewPreparationKit
{
    class Warm_up_Challenges
    {
        static int sockMerchant(int n, int[] ar)
        {
            int count = 0;

            Dictionary<int, int> dict = ar.GroupBy(g => g).ToDictionary(p => p.Key, p => p.Count());
            foreach (var item in dict)
            {
                count += item.Value / 2;
            }
            return count;

        }
        static int countingValleys(int n, string s)
        {
            int count = 0;
            int height = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == 'D')
                {
                    height--;
                }
                else
                {
                    height++;
                }
                if (height == -1 && s[i + 1] != 'D')
                {
                    count++;
                }
            }
            return count;

        }
        static long repeatedString(string s, long n)
        {
            long count = 0;
            foreach (var ch in s)
            {
                if (ch == 'a')
                    count++;
            }

            long rep = n / s.Length;
            count *= rep;
            var subStrLenght = n % s.Length;

            for (int i = 0; i < subStrLenght; i++)
            {
                if (s[i] == 'a')
                    count++;
            }
            return count;
        }
        static int jumpingOnClouds(int[] c)
        {

            int pos = 0;
            int count = 0;
            while (true)
            {
                if (pos + 2 <= c.Length - 1 && c[pos + 2] == 0)
                    pos = pos + 2;
                else
                    pos++;

                count++;
                if (pos == c.Length - 1)
                    break;
            }
            return count;
        }

    }
}
