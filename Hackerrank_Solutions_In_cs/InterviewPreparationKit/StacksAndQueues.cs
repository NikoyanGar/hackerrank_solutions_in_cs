﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Hackerrank_Solutions_In_cs.InterviewPreparationKit
{
    class StacksAndQueues
    {
        static string isBalanced(string chars)
        {
            Stack s = new Stack();
            foreach (char c in chars)
            {
                if (c == '{') s.Push('}');
                else if (c == '(') s.Push(')');
                else if (c == '[') s.Push(']');
                else
                {
                    if (s.Count == 0 || c != (char)s.Peek())
                        return "NO";
                    s.Pop();
                }
            }
            return s.Count == 0 ? "YES" : "NO";
        }
    }
}
