﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hackerrank_Solutions_In_cs.InterviewPreparationKit
{
    class HashTables
    {
        static void checkMagazine(string[] magazine, string[] note)
        {
            var n = note.GroupBy(s => s).ToDictionary(s => s.Key, s => s.Count());
            var m = magazine.GroupBy(s => s).ToDictionary(s => s.Key, s => s.Count());
            int count = 0;

            foreach (var item in n)
            {
                if (m.ContainsKey(item.Key))
                {
                    if (m[item.Key] < item.Value)
                    {
                        Console.WriteLine("No");
                        break;
                    }
                    else
                    {
                        count++;
                    }
                }
                else
                {
                    Console.WriteLine("No");
                    break;
                }
            }
            if (n.Count == count)
            {
                Console.WriteLine("Yes");
            }

        }

        
        static string twoStrings(string s1, string s2)
        {
            var s = s1.ToCharArray().Distinct();
            foreach (var item in s)
            {
                if (s2.Contains(item))
                {
                    return "YES";
                }
            }
            return "NO";

        }
    }
}
