﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hackerrank_Solutions_In_cs.InterviewPreparationKit
{
    class Arrays
    {
        static int hourglassSum(int[][] arr)
        {
            int[] sums = new int[16];
            int k = 0;
            for (int i = 1; i < 5; i++)
            {
                for (int j = 1; j < 5; j++)
                {
                    sums[k] = arr[i][j] + arr[i - 1][j] + arr[i - 1][j - 1] +
                        arr[i - 1][j + 1] + arr[i + 1][j] + arr[i + 1][j - 1] + arr[i + 1][j + 1];
                    k++;
                }
            }
            return sums.Max();

        }
        #region Array Manipulation

        static long arrayManipulation(int n, int[][] queries)
        {

            long[] outputArray = new long[n + 2];
            for (int i = 0; i < queries.Length; i++)
            {
                int a = queries[i][0];
                int b = queries[i][1];
                int k = queries[i][2];
                outputArray[a] += k;
                outputArray[b + 1] -= k;
            }
            long max = getMax(outputArray);
            return max;
        }


        private static long getMax(long[] inputArray)
        {
            long max = long.MinValue;
            long sum = 0;
            for (int i = 0; i < inputArray.Length; i++)
            {
                sum += inputArray[i];
                max = Math.Max(max, sum);
            }
            return max;
        }
        #endregion
        static int minimumSwaps(int[] arr)
        {
            //you need to compare the value in the array with the values that 
            //should be their place, if you need to put the falsity in the right place and hold that element in its place

            int numSwaps = 0;
            for (int i = 0; i < arr.Length - 1; i++)
            {
                int current = arr[i];
                while (current != i + 1)
                {
                    int temp = arr[current - 1];
                    arr[current - 1] = current;
                    current = temp;
                    numSwaps++;
                }
            }
            return numSwaps;
        }

    }
}
