﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Hackerrank_Solutions_In_cs.Problem_Solving.DataStructures
{
    class Arrays
    {
        //2D Array - DS
        static int hourglassSum(int[][] arr)
        {
            int[] sums = new int[16];
            int k = 0;
            for (int i = 1; i < 5; i++)
            {
                for (int j = 1; j < 5; j++)
                {
                    sums[k] = arr[i][j] + arr[i - 1][j] + arr[i - 1][j - 1] +
                        arr[i - 1][j + 1] + arr[i + 1][j] + arr[i + 1][j - 1] + arr[i + 1][j + 1];
                    k++;
                }
            }
            return sums.Max();

        }

        //Left Rotation
        public static void RotLeft(int[] sourse, int count)
        {
            //for modified indexes
            int[] indices = new int[sourse.Length];
            //so as not to swap in the source array
            int[] values = new int[sourse.Length];
            // don't make circles
            count %= sourse.Length;
            //Get  modified indexes
            for (int i = 0; i < sourse.Length; i++)
            {
                if (i - count < 0)
                    indices[i] = i - count + sourse.Length;

                else
                    indices[i] = i - count;

            }
            //Set values at modified indexes
            for (int i = 0; i < sourse.Length; i++)
            {
                values[indices[i]] = sourse[i];
            }
            //for print result

            //foreach (var item in values)
            //{
            //    Console.Write(item + " ");
            //}
        }

        //Sparse Arrays
        static int[] matchingStrings(string[] strings, string[] queries)
        {
            int[] counts = new int[queries.Length];
            int i = 0;
            foreach (var query in queries)
            {
                foreach (var str in strings)
                {
                    if (query == str)
                    {
                        counts[i]++;
                    }
                }
                i++;
            }
            return counts;

        }

        #region Array Manipulation
        //Terminated due to timeout :(
        static long arrayManipulation(int n, int[][] queries)
        {
            long[] results = new long[n];
            int a;
            int b;
            int k;
            for (int i = 0; i < queries.Length; i++)
            {

                a = queries[i][0] - 1;
                b = queries[i][1] - 1;
                k = queries[i][2];
                for (int j = a; j <= b; j++)
                {
                    results[j] += k;
                }
            }
            return results.Max();
        }
        //Terminated due to timeout :(
        //static void Main(string[] args)
        //{
        //    TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        //    string[] nm = Console.ReadLine().Split(' ');

        //    int n = Convert.ToInt32(nm[0]);

        //    int m = Convert.ToInt32(nm[1]);

        //    long[] results = new long[n];

        //    for (int i = 0; i < m; i++)
        //    {
        //        string[] opString = Console.ReadLine().Split(' ');
        //        int a = Int32.Parse(opString[0]) - 1;
        //        int b = Int32.Parse(opString[1]) - 1;
        //        long k = long.Parse(opString[2]);
        //        //reason for the delay(100000a 100000b interval for a and b(1400906 9889280 ))
        //        //hiden case 7 data   https://hr-testcases-us-east-1.s3.amazonaws.com/1636/input07.txt?AWSAccessKeyId=AKIAJ4WZFDFQTZRGO3QA&Expires=1586519196&Signature=rQyDENLkG0HIjxgIUdf8gPoj0Rk%3D&response-content-type=text%2Fplain
        //        for (int j = a; j <= b; j++)
        //        {
        //            results[j] += k;
        //        }
        //    }
        //    long max = results.Max();



        //    textWriter.WriteLine(max);

        //    textWriter.Flush();
        //    textWriter.Close();
        //}

        // Kanahaiya Gupta
        static long ArrayManipulation(int n, int[][] queries)
        {

            long[] outputArray = new long[n + 2];
            for (int i = 0; i < queries.Length; i++)
            {
                int a = queries[i][0];
                int b = queries[i][1];
                int k = queries[i][2];
                outputArray[a] += k;
                outputArray[b + 1] -= k;
            }
            long max = getMax(outputArray);
            return max;
        }


        private static long getMax(long[] inputArray)
        {
            long max = long.MinValue;
            long sum = 0;
            for (int i = 0; i < inputArray.Length; i++)
            {
                sum += inputArray[i];
                max = Math.Max(max, sum);
            }
            return max;
        }

        #endregion
    }
}
