﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hackerrank_Solutions_In_cs.Problem_Solving.DataStructures
{
    class LinkedLists
    {
        static void printLinkedList(SinglyLinkedListNode head)
        {

            while (true)
            {
                Console.WriteLine(head.data);
                if (head.next == null)
                    break;
                head = head.next;

            }

        }
        public class SinglyLinkedListNode
        {
            public int data;
            public SinglyLinkedListNode next;
        }
    }
}
