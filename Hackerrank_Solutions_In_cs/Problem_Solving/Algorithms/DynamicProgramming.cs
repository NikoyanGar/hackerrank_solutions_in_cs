﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hackerrank_Solutions_In_cs.Problem_Solving.Algorithms
{
    class DynamicProgramming
    {
        //Sherlock and Cost
        public static int cost(int[] B)
        {
            int[] c = new int[2];
            int[] c1 = new int[2];
            for (int i = 1; i < B.Length; i++)
            {
                c[0] = Math.Max(c1[0] + Math.Abs(B[i] - B[i - 1]), c1[1] + Math.Abs(B[i] - 1));
                c[1] = Math.Max(c1[0] + Math.Abs(B[i - 1] - 1), c1[1]);

                Array.Copy(c, c1, 2);
            }
            return Math.Max(c[0], c[1]);
        }
    }
}
