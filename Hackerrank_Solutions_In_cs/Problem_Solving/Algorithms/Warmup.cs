﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hackerrank_Solutions_In_cs.Problem_Solving.Algorithms
{
    class Warmup
    {
        static int simpleArraySum(int[] ar)
        {
            int sum = ar[0];
            for (int item = 1; item < ar.Length; item++)
            {
                sum += ar[item];
            }
            return sum;
        }

        static List<int> compareTriplets(List<int> a, List<int> b)
        {

            List<int> c = new List<int>(2) { 0, 0 };
            for (int i = 0; i < a.Count; i++)
            {
                if (a[i] > b[i])
                {
                    c[0]++;
                }
                else if (a[i] < b[i])
                {
                    c[1]++;
                }
            }
            return c;
        }
        
        static long aVeryBigSum(long[] ar)
        {
            long sum = 0;
            foreach (long item in ar)
            {
                sum += item;
            }
            return sum;


        }
        public static int diagonalDifference(List<List<int>> arr)
        {
            int sum1 = 0;
            int sum2 = 0;
            /* (0,0)(0,1)(0,2)
             * (1,0)(1,1)(1,2)
             * (2,0)(2,1)(2,2)
             * i=0,j=count-1,
             * i=1,j=count-1-1,  =>j=count-1-i:
             * i=2,j=count-1-2,
             */
            for (int i = 0; i < arr.Count; i++)
            {
                for (int j = 0; j < arr[i].Count; j++)
                {
                    if (i == j)
                    {
                        sum1 += arr[i][j];
                    }
                    if (j == arr[i].Count - 1 - i)
                    {
                        sum2 += arr[i][j];
                    }
                }
            }
            return Math.Abs(sum1 - sum2);

        }
        static void plusMinus(int[] arr)
        {
            int countP = 0;
            int count0 = 0;
            int countM = 0;

            foreach (var item in arr)
            {
                if (item > 0)
                {
                    countP++;
                }
                else if (item < 0)
                {
                    countM++;
                }
                else
                {
                    count0++;
                }
            }

            double d = (double)countP / arr.Length;
            string s = d.ToString("0.00000");
            double f = (double)countM / arr.Length;
            string g = f.ToString("0.00000");
            double l = (double)count0 / arr.Length;
            string m = l.ToString("0.00000");
            Console.WriteLine(s);
            Console.WriteLine(g);
            Console.WriteLine(m);


        }
        static void staircase(int n)
        {

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n - 1 - i; j++)
                {
                    Console.Write(" ");
                }
                for (int k = 0; k < i + 1; k++)
                {
                    Console.Write("#");
                }
                Console.WriteLine();
            }
        }
        static void miniMaxSum(int[] arr)
        {
            int max = arr[0];
            int indmax = 0;
            int indmin = 0;
            int min = arr[0];
            long summax = 0;
            long summin = 0;
            for (int i = 0; i < 5; i++)

            {
                if (arr[i] > max)
                {

                    max = arr[i];
                    indmax = i;
                }

                if (arr[i] < min)
                {
                    min = arr[i];
                    indmin = i;
                }

            }
            for (int j = 0; j < 5; j++)
            {
                if (j == indmax)
                    continue;
                summin += arr[j];
            }
            for (int k = 0; k < 5; k++)
            {
                if (k == indmin)
                    continue;
                summax += arr[k];
            }
            Console.WriteLine($"{summin} {summax}");
        }
        static int birthdayCakeCandles(int[] ar)
        {
            int max = 0;
            foreach (var item in ar)
            {
                if (item > max)
                {
                    max = item;
                }
            }
            int count = 0;
            foreach (var item in ar)
            {
                if (item == max)
                {
                    count++;
                }
            }
            return count;

        }
        static string timeConversion(string s)
        {
            if (s[8] == 'P')
            {
                s = s.Remove(8);
                string str = s.Substring(0, 2);
                int h = Convert.ToInt32(str);
                h += 12;
                s = s.Replace(str, h.ToString());
            }
            else
            {
                s = s.Remove(8);
            }
            return s;
        }

    }
}
