﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hackerrank_Solutions_In_cs.Problem_Solving.Algorithms
{
    class Search
    {
        static List<int> missingNumbers(int[] arr, int[] brr)
        {
            List<int> b = brr.ToList();
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < b.Count; j++)
                {
                    if (arr[i] == b[j])
                    {
                        b.RemoveAt(j);
                        break;
                    }
                }
            }

            return b.Distinct().OrderBy(p => p).ToList();
        }
        static int pairs(int k, int[] arr)
        {
            Dictionary<int, int> dict = arr.GroupBy(p => p)
                .OrderBy(p => p.Key)
                .ToDictionary(p => p.Key, p1 => p1.Count());//voretex key eri tarberutyun@ =k)countner@ bazmapatkel
            int count = 0;
            foreach (var item in dict)
            {
                if (dict.ContainsKey(item.Key + k))
                {
                    count += item.Value;
                }

            }
            return count;
        }
       // Sherlock and Array
        static string balancedSums(List<int> arr)
        {
            arr.RemoveAll(p => p == 0);

            if (arr.Count == 1)
            {
                return "YES";
            }
            if (arr.Count == 2)
            {
                return "NO";
            }
            long sumBefore = arr[0];
            long sumAfter = arr.Sum() - arr[0] - arr[1];
            int index = 1;
            while (sumAfter > sumBefore)
            {

                sumAfter -= arr[index + 1];
                sumBefore += arr[index];
                index++;
            }
            if (sumBefore == sumAfter)
                return "YES";

            return "NO";
        }

    }
}
