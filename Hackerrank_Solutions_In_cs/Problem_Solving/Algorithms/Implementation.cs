﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace Hackerrank_Solutions_In_cs.Problem_Solving.Algorithms
{
    class Implementation
    {

        public static int pickingNumbers(List<int> a)
        {
            var dict = a.GroupBy(n => n).ToDictionary(n => n.Key, n => n.Count());
            int max = 0;
            foreach (var item in dict)
            {
                int count;
                dict.TryGetValue(item.Key + 1, out count);
                int c = item.Value + count;
                if (c > max)
                {
                    max = c;
                }
            }
            return max;

        }
        #region encryption
        //optimized
        static string encryption(string s)
        {

            s = s?.Replace(" ", "");
            double d = Math.Sqrt(s.Length);
            int rowsCount = (int)d;
            int columnCount = (int)d;
            if (d - rowsCount > 0)
            {
                columnCount++;
            }

            int x = s.Length;
            StringBuilder str = new StringBuilder(s.Length);
            for (int i = 0; i < columnCount; i++)
            {
                int k = i;
                while (true)
                {
                    str.Append(s[k]);
                    k += columnCount;
                    if (k >= s.Length)
                    {
                        break;
                    }
                }

                str.Append(" ");
            }
            return str.ToString();
        }
        //with matrix
        static string encryption1(string s)
        {
            s = s?.Replace(" ", "");
            double d = Math.Sqrt(s.Length);
            int rowsCount = (int)d;
            int columnCount = (int)d;
            if (d - rowsCount > 0)
            {
                columnCount++;
            }
            if (rowsCount * columnCount < s.Length)
            {
                rowsCount++;
            }
            int k = 0;
            char[,] encript = new char[rowsCount, columnCount];
            for (int i = 0; i < rowsCount; i++)
            {
                for (int j = 0; j < columnCount; j++)
                {
                    if (k < s.Length)
                    {
                        encript[i, j] = s[k];
                    }
                    k++;

                }
            }
            string newStr = "";

            for (int i = 0; i < columnCount; i++)
            {
                for (int j = 0; j < rowsCount; j++)
                {
                    if (encript[j, i] != '\0')
                    {
                        newStr += encript[j, i];
                    }

                }
                newStr += " ";
            }

            return newStr;
        }
        #endregion
        public static List<int> gradingStudents(List<int> grades)
        {
            for (int i = 0; i < grades.Count; i++)
            {
                if (grades[i] < 38)
                {
                    continue;
                }
                if (grades[i] % 5 != 0 && grades[i] % 5 >= 3)
                {
                    grades[i] += 5 - (grades[i] % 5);
                }
            }
            return grades;
        }
        static void countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges)
        {
            int countApple = 0;
            int countOrande = 0;

            foreach (int apple in apples)
            {
                if (a + apple <= t && a + apple >= s)
                {
                    countApple++;
                }
            }
            foreach (int orange in oranges)
            {
                if (b + orange >= s && b + orange <= t)
                {
                    countOrande++;
                }
            }
            Console.WriteLine(countApple);
            Console.WriteLine(countOrande);
        }
        static int[] breakingRecords(int[] scores)
        {
            int[] counts = new int[] { 0, 0 };
            int minValue = scores[0];
            int maxValue = scores[0];
            for (int i = 1; i < scores.Length; i++)
            {
                if (scores[i] < minValue) { counts[1]++; minValue = scores[i]; }
                if (scores[i] > maxValue) { counts[0]++; maxValue = scores[i]; }
            }
            return counts;

        }
        static int birthday(List<int> s, int d, int m)
        {
            int sum = 0;
            int count = 0;
            for (int j = 0; j < s.Count; j++)
            {
                for (int i = 0; i < m; i++)
                {
                    if (i + j == s.Count)
                    {
                        break;
                    }
                    sum += s[i + j];

                }
                if (sum == d)
                {
                    count++;
                }
                sum = 0;
            }


            return count;
        }
        static int divisibleSumPairs(int n, int k, int[] ar)
        {
            int count = 0;
            for (int i = 0; i < ar.Length - 1; i++)
            {
                for (int j = i + 1; j < ar.Length; j++)
                {
                    if ((ar[i] + ar[j]) % k == 0)
                    {
                        count++;
                    }
                }
            }
            return count;
        }
        static int migratoryBirds(List<int> arr)
        {


            return arr.GroupBy(g => g)
                 .ToDictionary(g => g.Key, g => g.Count())
                 .OrderByDescending(o => o.Value).ThenBy(o => o.Key).FirstOrDefault().Key;
        }
        static void bonAppetit(List<int> bill, int k, int b)
        {
            bill.RemoveAt(k);
            int sum = bill.Sum() / 2;
            if (sum == b)
            {
                Console.WriteLine("Bon Appetit");
            }
            else
            {
                Console.WriteLine(b - sum);
            }

        }
        static int sockMerchant(int n, int[] ar)
        {
            int count = 0;

            Dictionary<int, int> dict = ar.GroupBy(g => g).ToDictionary(p => p.Key, p => p.Count());
            foreach (var item in dict)
            {
                count += item.Value / 2;
            }
            return count;

        }
        static int countingValleys(int n, string s)
        {
            int count = 0;
            int height = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == 'D')
                {
                    height--;
                }
                else
                {
                    height++;
                }
                if (height == -1 && s[i + 1] != 'D')
                {
                    count++;
                }
            }
            return count;

        }
        static int getMoneySpent(int[] keyboards, int[] drives, int b)
        {
            int max = 0;
            foreach (var pkeybord in keyboards)
            {
                foreach (var pdrive in drives)
                {
                    int totalPrice = pkeybord + pdrive;
                    if (totalPrice > max && totalPrice <= b)
                    {
                        max = totalPrice;
                    }
                }
            }
            if (max == 0)
                return -1;
            return max;
        }
        static string catAndMouse(int x, int y, int z)
        {
            int dist1 = Math.Abs(z - x);
            int dist2 = Math.Abs(z - y);
            if (dist1 == dist2)
                return "Mouse C";
            if (dist1 < dist2)
                return "Cat A";
            return "Cat B";

        }
        static int hurdleRace(int k, int[] height)
        {

            if (height.Max() < k) return 0;

            return height.Max() - k;
        }
        static string angryProfessor(int k, int[] a)
        {
            int count = 0;
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] <= 0)
                {
                    count++;
                }
            }
            if (count >= k)
            {
                return "NO";
            }
            else
            {
                return "YES";
            }


        }
        static int[] permutationEquation(int[] p)
        {
            int[] arr = new int[p.Length];
            for (int i = 1; i < p.Length + 1; i++)
            {
                int x = Array.IndexOf(p, i) + 1;
                int y = Array.IndexOf(p, x) + 1;
                arr[i - 1] = y;
            }
            return arr;

        }
        #region jumpingOnClouds
        static int jumpingOnClouds(int[] c, int k)
        {
            int e = 100;
            int pos = k;
            if (k == c.Length)
            {
                if (c[0] == 1)
                    return e -= 3;
                else
                    return e -= 1;
            }
            while (true)
            {
                if (pos != 0)
                {
                    if (c[pos] == 1)
                        e -= 3;
                    else
                        e -= 1;

                }
                else
                {
                    if (c[pos] == 1)
                        return e -= 3;
                    else
                        return e -= 1;
                }
                if (pos + k > c.Length - 1)
                    pos = pos + k - c.Length;
                else
                    pos += k;


            }
        }
        #endregion
        static int findDigits(int n)
        {
            String s = $"{n}";
            int count = 0;
            foreach (var item in s)
            {
                if (item == '0')
                    continue;
                int rem = n % (item - '0');
                if (rem == 0)
                    count++;
            }
            return count;

        }
        static void extraLongFactorials(int n)
        {

            BigInteger fact;

            fact = n;
            for (int i = n - 1; i >= 1; i--)
            {
                fact = fact * i;
            }
            Console.WriteLine(fact);

        }
        //Sherlock and Squares
        static int squares(int a, int b)
        {
            int count = 0;

            for (int i = (int)Math.Sqrt(a); i <= (int)Math.Sqrt(b); i++)
            {
                if (i * i >= a && i * i <= b)
                {
                    count++;
                }
            }
            return count;
        }
        static int libraryFine(int d1, int m1, int y1, int d2, int m2, int y2)
        {
            int y3 = y1 - y2;
            int m3 = m1 - m2;
            int d3 = d1 - d2;
            if (y3 >= 0)
            {
                if (y3 > 0)
                {
                    return y3 * 10000;
                }
                else if (m3 >= 0)
                {
                    if (m3 > 0)
                    {
                        return m3 * 500;
                    }
                    else if (d3 >= 0)
                    {
                        if (d3 > 0)
                        {
                            return d3 * 15;
                        }

                    }
                }

            }

            return 0;

        }
        static int[] cutTheSticks(int[] arr)
        {
            var items = arr.ToList().GroupBy(g => g).OrderBy(o => o.Key).ToList();
            List<int> counts = new List<int>();
            for (int i = 0; i < items.Count(); i++)
            {
                int count = 0;
                for (int j = i; j < items.Count(); j++)
                {
                    count += items[j].Count();
                }
                counts.Add(count);
            }

            return counts.ToArray();
        }
        #region nonDivisibleSubset
        public static int nonDivisibleSubset(int k, List<int> s)
        {
            if (s.Count == 1)
            {
                return 1;
            }
            int max = 0;
            bool IsIn = false;
            bool IsIn1 = false;

            List<int> mnacordner = new List<int>();
            for (int i = 0; i < s.Count; i++)
            {
                int mnacord = s[i] % k;
                if (mnacord == 0)
                {
                    IsIn = true;
                }
                else if (mnacord == (double)k / 2)
                {
                    IsIn1 = true;
                }
                else
                {
                    mnacordner.Add(mnacord);  //7k+1,7k+2,7k+3,7k+4,7k+5,7+6

                }
            }

            int[] uniqe = mnacordner.Distinct().ToArray();
            Array.Sort(uniqe);
            int[] counts = new int[uniqe.Length];
            for (int i = 0; i < uniqe.Length; i++)
            {
                foreach (var mnacord in mnacordner)
                {
                    if (uniqe[i] == mnacord)
                    {
                        counts[i]++;
                    }
                }
            }
            for (int i = 0; i < uniqe.Length; i++)
            {
                for (int j = 1; j < uniqe.Length; j++)
                {
                    if (uniqe[i] + uniqe[j] == k && counts[i] + counts[j] != 0)
                    {
                        if (counts[i] > counts[j])
                        {
                            max += counts[i];
                            counts[i] = 0;
                            counts[j] = 0;
                            break;


                        }
                        else
                        {
                            max += counts[j];
                            counts[i] = 0;
                            counts[j] = 0;
                            break;
                        }
                    }
                }
                max += counts[i];
            }
            if (IsIn)
            {
                max++;
            }
            if (IsIn1)
            {
                max++;
            }
            return max;
        }
        #endregion
        static int equalizeArray(int[] arr)
        {
            int res = arr.GroupBy(g => g)
               .ToDictionary(g => g.Key, g => g.Count())
               .Select(s => s.Value).OrderByDescending(o => o).FirstOrDefault();

            return arr.Length - res;

        }
        public static long taumBday(long b, long w, long bc, long wc, long z)
        {
            long sum = 0;

            if (bc > wc + z)
            {
                sum = w * wc + b * (wc + z);
            }
            else if (wc > bc + z)
            {
                sum = b * bc + w * (bc + z);
            }
            else
            {
                sum = b * bc + w * wc;
            }

            return sum;
        }
        static int minimumDistances(int[] arr)
        {

            int min = arr.Length;
            for (int i = 0; i < arr.Length - 1; i++)
            {
                int dist = arr.Length;
                for (int j = i + 1; j < arr.Length; j++)
                {
                    if (arr[i] == arr[j])
                    {
                        dist = j - i;
                        if (dist < min)
                        {
                            min = dist;
                        }
                    }
                }
            }
            if (min == arr.Length)
            {
                return -1;
            }
            return min;
        }
        //Halloween Sale
        static int howManyGames(int p, int d, int m, int s)
        {
            int count = 0;
            int courentPrice = p;
            bool canBuyNext = true;
            if (p > s)
            {
                return 0;
            }
            while (canBuyNext)
            {
                s -= courentPrice;
                if (courentPrice - d > m)
                {
                    courentPrice = courentPrice - d;

                }
                else
                {

                    courentPrice = m;

                }
                if (s - courentPrice < 0)
                {
                    canBuyNext = false;
                }
                count++;


            }
            return count;


        }
        #region timeInWords
        static string timeInWords(int h, int m)
        {
            string time = "";
            string[] hours = new[] { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven" };
            string[] minutes = new[] { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"
                                    , "eleven", "twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen","twenty"
                                    , "twenty one", "twenty two", "twenty three", "twenty four", "twenty five",
                                        "twenty six", "twenty seven", "twenty eight","twenty nine" };
            if (m == 0)
                time = $"{hours[h - 1]} o' clock";

            if ((m > 1 && m < 15) || (m > 15 && m < 30))
                time = $"{minutes[m - 1]} minutes past {hours[h - 1]}";

            if ((m > 30 && m < 45) || (m > 45 && m < 60))
                time = $"{minutes[60 - m - 1]} minutes to {hours[h]}";


            if (m == 1)
                time = $"{minutes[m - 1]} minute past {hours[h - 1]}";

            if (m == 15)
                time = $"quarter past {hours[h - 1]}";

            if (m == 30)
                time = $"half past {hours[h - 1]}";

            if (m == 45)
                time = $"quarter to {hours[h]}";

            return time;

        }
        #endregion
        static int utopianTree(int n)
        {
            int height = 1;
            int cikl = 0;
            while (n != 0)
            {
                cikl++;
                if (cikl % 2 == 1)
                {
                    height *= 2;
                }
                else
                {
                    height++;
                }
                n--;
            }
            return height;

        }
        static string[] cavityMap(string[] grid)
        {
            for (int i = 1; i < grid.Length - 1; i++)
            {
                bool any = false;//1
                char[] chars = grid[i].ToArray();//2

                for (int j = 1; j < grid[i].Length - 1; j++)
                {
                    if (grid[i][j] > grid[i][j - 1] && grid[i][j] > grid[i][j + 1] &&
                        grid[i][j] > grid[i - 1][j] && grid[i][j] > grid[i + 1][j])
                    {
                        chars[j] = 'X';//3
                        any = true;//4
                    }
                }
                if (any)
                {
                    grid[i] = new string(chars);//5
                }
            }
            //grid[i].Remove(j, 1).Insert(j, "X");
            return grid;

        }
    }

}

