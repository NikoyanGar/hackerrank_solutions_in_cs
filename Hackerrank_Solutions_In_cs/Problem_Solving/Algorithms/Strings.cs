﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Hackerrank_Solutions_In_cs.Problem_Solving.Algorithms
{
    class Strings
    {
        static string superReducedString(string s)
        {

            for (int i = 0; i < s.Length - 1; i++)
            {
                if (s[i] == s[i + 1])
                {
                    s = s.Remove(i, 2);
                    i = -1;
                }
            }
            if (s.Length == 0)
                return "Empty String";

            return s;
        }
        static int camelcase(string s)
        {

            return s.Where(p => Char.IsUpper(p)).Count() + 1;
        }
        static int minimumNumber(int n, string password)
        {
            int count;
            Regex regex = new Regex(@"[-!@#$%^&*()+]");
            Regex regex1 = new Regex(@"\d");
            Regex regex2 = new Regex(@"[a-z]");
            Regex regex3 = new Regex(@"[A-Z]");

            Match match = regex.Match(password);
            Match match1 = regex1.Match(password);
            Match match2 = regex2.Match(password);
            Match match3 = regex3.Match(password);

            bool[] vs = new bool[4] { match.Success, match1.Success, match2.Success, match3.Success };
            count = vs.Where(i => i.ToString() == "False").Count();
            while (n + count < 6)
            {
                count++;
            }
            return count;

        }
        static string caesarCipher(string s, byte k)
        {
            k = Convert.ToByte(k % 26);
            ASCIIEncoding aSCII = new ASCIIEncoding();
            byte[] b = aSCII.GetBytes(s);
            for (int i = 0; i < b.Length; i++)
            {
                if (97 <= b[i] && b[i] <= 122)
                {
                    if (b[i] + k > 122)
                        b[i] = Convert.ToByte(b[i] + k - 26);
                    else
                        b[i] += k;
                }
                else if (65 <= b[i] && b[i] <= 90)
                {
                    if (b[i] + k > 90)
                        b[i] = Convert.ToByte(b[i] + k - 26);
                    else
                        b[i] += k;
                }

            }
            return aSCII.GetString(b);

        }
        static int marsExploration(string s)
        {
            int count = 0;
            string[] strs = new string[s.Length / 3];
            for (int i = 0; i < strs.Length; i++)
            {
                strs[i] = s.Substring(i * 3, 3);
            }
            foreach (var item in strs)
            {
                if (item[0] != 'S')
                {
                    count++;
                }
                if (item[2] != 'S')
                {
                    count++;
                }
                if (item[1] != 'O')
                {
                    count++;
                }
            }

            return count;

        }
        static string pangrams(string s)
        {
            int count = s.ToUpper().Where(p => Char.IsLetter(p)).Distinct().Count();
            if (count == 26)
            {
                return "pangram";
            }
            return "not pangram";

        }
        static string funnyString(string s)
        {
            for (int i = 0; i < s.Length / 2; i++)
            {
                int l = (s.Length - 1) - i;
                int a = (int)s[i];
                int b = (int)s[i + 1];
                int c = (int)s[l];
                int d = (int)s[l - 1];

                if (Math.Abs(a - b) != Math.Abs(c - d))
                    return "Not Funny";
            }
            return "Funny";
        }

        static int alternatingCharacters(string s)
        {
            int count = 0;
            for (int i = 0; i < s.Length - 1; i++)
            {
                if (s[i] == s[i + 1])
                {
                    count++;
                }
            }
            return count;

        }
    }
}
