﻿using System;
using System.Linq;

namespace Hackerrank_Solutions_In_cs
{



    class Program
    {

        static void Main(String[] args)
        {

            int fine = 0;
            int[] actual = Console.ReadLine().Split().Select(int.Parse).ToArray();
            int[] expected = Console.ReadLine().Split().Select(int.Parse).ToArray();
           

            if (actual[2] > expected[2])
                fine = 10000;

            else if (actual[1] > expected[1] && actual[2]== expected[2])
                fine = (actual[1] - expected[1]) * 500;

            else if (actual[0] > expected[0] && actual[2] == expected[2] && actual[1] == expected[1])
                fine = (actual[0] - expected[0]) * 15;


            Console.WriteLine(fine);

        }


    }
}